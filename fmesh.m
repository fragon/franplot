%FMESH Plots a set of data from a Z(X,Y) function as a mesh.
%
%   FMESH(X_MAT, Y_MAT, Z_MAT) plots the function Z_MAT represented by the matrix Y_MAT for Y
%   axis and X_MAT for X axis.
%   FMESH(X_MAT, Y_MAT, Z_MAT, MODIFIER) called with four arguments
%   represents the function with the style specified in MODIFIER using MATLAB standard
%   format
%   FMESH(X_MAT, Y_MAT, Z_MAT, MODIFIER, LINEWIDTH) called with five arguments
%   represents the function with the specified modified and line width
%   specified in LINEWIDTH
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fmesh(x_mat, y_mat, z_mat, modifier, linewidth)

% Create plot
if(nargin == 3)
    mesh(x_mat,y_mat, z_mat,'LineWidth',1.5);
elseif(nargin == 4)
    mesh(x_mat,y_mat, z_mat,modifier,'LineWidth',1.5);
elseif(nargin == 5)
    mesh(x_mat,y_mat, z_mat,modifier,'LineWidth',linewidth);
end

axis([-inf, inf,-inf,inf,-inf,inf]);
shg;
end