%FLEGEND Set the legend specify in a cell
%
%   FLEGEND(LEGEND_CELL) set the legend for the active plot to the one
%   specified in LEGEND_CELL cell.
%   FLEGEND(LEGEND_CELL, LOCATION) called with two arguments, set the
%   legend for active plot in the location specified in LOCATION string.
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function flegend(legend_cell, location)

if nargin == 1
    legend(legend_cell,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');
else
    legend(legend_cell,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex', 'location', location);
end

end