%FPLOT Plot a set of data from a Y(X) curve
%
%   FPLOT(X_VECT, Y_VECT) plots the curve represented by the vector Y_VECT for Y
%   axis and X_VECT for X axis.
%   FPLOT(X_VECT, Y_VECT, MODIFIER) called with three argument represent
%   the curve with the style specified in MODIFIER using MATLAB standard
%   format
%   FPLOT(X_VECT, Y_VECT, MODIFIER, LINEWIDTH) called with four arguments
%   represent the curve with the specified modified and line width
%   specified in LINEWIDTH
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fplot(x_vect, y_vect, modifier, linewidth)

% Create plot
if(nargin == 2)
    plot(x_vect,y_vect,'LineWidth',1);
elseif(nargin == 3)
    plot(x_vect,y_vect,modifier,'LineWidth',1);
elseif(nargin == 4)
    plot(x_vect,y_vect,modifier,'LineWidth',linewidth);
end

axis([-inf, inf,-inf,inf]);
shg;
end