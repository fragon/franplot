%FMOD Apply IEEE style to a created figure
%
%   FMOD collects data from an existing figure and applies the IEEE
%   recomendations for publications.
%   FMOD() (called with no argument) applies the modification to  the current
%   figure handle (gcf).
%   FMOD(HANDLE) receives an arbitrary figure handle stored in the variable
%   to change the style of it to be set for the recommendations 
%   of IEEE publications,
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fmod(h_fig)

if(nargin == 0)
    h_fig = gcf;
end
%h_axes = get(h_fig,'CurrentAxes');

% Extract figure data fron handle
N = numel(h_fig.Children);
pos1 = zeros (N,1); pos2 = zeros(N,1);
for n = 1:N
    pos1(n) = h_fig.Children(n).Position(1);
    pos2(n) = h_fig.Children(n).Position(2);
end
num_y = numel(unique(pos1));
num_x = numel(unique(pos2));

%Do the changes to the window (as in FINIT)
set(h_fig,'InvertHardcopy','off','Color',[1 1 1]);
set(h_fig,'Units','centimeters');
set(h_fig,'Position',[0, 0 17.6*((num_y-1)*0.5+1) 10.88*((num_x-1)*0.5+1)]);
h_fig.PaperUnits = 'centimeters';
h_fig.PaperPosition = [0 0 17.6*((num_y-1)*0.5+1) 10.88*((num_x-1)*0.5+1)];
movegui(h_fig, 'center');

allAxes = findall(h_fig,'type','axes');

for i = 1:length(allAxes)
    
    h_axes = allAxes(i);
    box(h_axes,'on');
    set(h_axes,'FontName','times new roman','FontSize',16,'LineWidth',1,'XGrid',...
        'on','YGrid','on', 'GridAlpha', 0.3,'GridLineStyle', '--','TickLabelInterpreter', 'latex');
    hold(h_axes,'on');

    %Do the changes in the title (similar to FTITLE)
    title_handle = h_axes.Title;
    set(title_handle,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');
    
    %Do the changes to the legend (if applicable, similar to FLEGEND)
    legend_cell = findobj(h_fig.Children, 'Type', 'Legend');
    if ~isempty(legend_cell)
        set(legend_cell,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');
    end
    
    
    %Do the changes to the contour (if applicable, similar to FCONTOURS)
    contObjs = get(h_axes, 'Children');
    if ~isempty(contObjs)
        set(contObjs,'LineWidth',1);
        clabel(contObjs.ContourMatrix,contObjs,'FontSize',11,'FontName','times new roman')
    end
    
    %Do the changes to the labels (similar to FLABEL)
    x_lab_hand = get(h_axes, 'xlabel');
    y_lab_hand = get(h_axes, 'ylabel');
    z_lab_hand = get(h_axes, 'zlabel');
    set(x_lab_hand,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');
    set(y_lab_hand,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');
    set(z_lab_hand,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');
   
    %Change line width (similar to FPLOT)
    h_line = findobj(h_axes, 'type', 'line');
    set(h_line,'LineWidth', 1.5);
    axis(h_axes, [-inf,inf,-inf,inf]);
end
    
shg;
end