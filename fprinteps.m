%FPRINTEPS Save the current figure and export for publication in EPS
%
%   FPRINTEPS(FILENAME) saves the current figure in FIG format, and export 
%   to EPS for publication using the filename specified in FILENAME. 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fprinteps(filename)

eval(['export_fig ', filename,' -eps']);

savefig([filename, '.fig'])