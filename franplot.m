%FRANPLOT 'Plot with style, IEEE style ;)'
%
%   This is a help funtion for FRANPLOT. Here is a summary of how to use it
%
%   To start a new figure, use FINIT in 2D or FINIT3 in 3D
%
%   To apply IEEE style to an already existing plot, use FMOD
%
%   To plot data, use FPLOT if using X-Y datasets,
%   FPLOT3 for X-Y-Z curve datasets,
%   FPLOTS if using 'structure with time' datasets,
%   FMESH for Z-XY mesh datasets,
%   FSURF for Z-XY surfaces datasets or
%   FCONTOURS for Z-XY contour plot.
%
%   To adjust axis limits or 3D view, use FAXIS
%
%   To add labels to the active plot, use FLABELS
%
%   To add a title, use FTITLE
%
%   To add a text annotation over a graph. use FTEXT
%
%   To add a legend, use FLEGEND for a one-column legend or
%   FCOLUMNLEGEND for a N-columns legend.
%
%   To save and export your figure use:
%   FPRINTPDF, to save in FIG and export in PDF
%   FPRINTEPS, to save in FIG and export in EPS
%   FPRINTEMF, to save in FIG and export in EMF
%   FPRINTPNG, to save in FIG and export in PNG
%
%   For developing or learning purposes, a demostration of most of the
%   features can be executed in FRANTEST script.
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin
