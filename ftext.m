%FTEXT Adds a text over a certain plot at a desired place or at its center.
%
%   FTEXT(X_VECT, Y_VECT, TEXT_STR) writes the text string in TEXT_STR over
%   the plot represented by X_VECT / Y_VECT pair, at the center of the
%   axes. If X_VECT / Y_VECT is a single value, text is located at the
%   given point.
%   FTEXT(X_VECT, Y_VECT, TEXT_STR, SIZE), called with four arguments,
%   represent the text with the font size specified in SIZE.
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function ftext(x_vect, y_vect, text_str, size)
i = round(numel(x_vect)/2);

if nargin == 3
    size = 9;
end

text(x_vect(i), y_vect(i), text_str, 'BackgroundColor', 'w', 'LineWidth',1,'FontSize',size,'FontName','times new roman', 'Interpreter', 'latex');
end