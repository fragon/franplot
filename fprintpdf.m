%FPRINTPDF Save the current figure and export for publication in PDF
%
%   FPRINTPDF(FILENAME) saves the current figure in FIG format, and export 
%   to PDF for publication using the filename specified in FILENAME. 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fprintpdf(filename)

eval(['export_fig ', filename,' -pdf']);

savefig([filename, '.fig'])