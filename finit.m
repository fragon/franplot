%FINIT Initialize plot with IEEE style
%
%   FINIT initiate a 1-by-1 figure. Figure is set for the recomendations
%   of IEEE publications.
%   FINIT(NUM_X, NUM_Y, INDEX) initialize a NUM_X-by-NUM_Y figure and 
%   activates ploting on the INDEX subplot. Figure style is set for the 
%   recommendations of IEEE publications, 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function finit(num_x, num_y, index)
if nargin == 0
    num_x = 1; num_y = 1; index = 1;
end

if index == 1
    % Create figure
    figure1 = figure('InvertHardcopy','off','Color',[1 1 1]);
    set(figure1,'Units','centimeters');
    set(figure1,'Position',[0, 0 17.6*((num_y-1)*0.5+1) 10.88*((num_x-1)*0.5+1)]);
    figure1.PaperUnits = 'centimeters';
    figure1.PaperPosition = [0 0 17.6*((num_y-1)*0.5+1) 10.88*((num_x-1)*0.5+1)];
    movegui(figure1, 'center');
end
axes1 = subplot(num_x, num_y, index);

box(axes1,'on');
% Set the remaining axes properties
set(axes1,'FontName','times new roman','FontSize',16,'LineWidth',1,'XGrid',...
    'on','YGrid','on', 'GridAlpha', 0.12,'GridLineStyle', '--','TickLabelInterpreter', 'latex');
hold(axes1,'on');
end