%FPLOTS Plot a set of data from a structure with time dataset
%
%   FPLOTS(STRUCT_DATA) plots the curve represented by the
%   structure with time using Simulink 'Send to workspace' format. Called
%   with single argument plots all the datasets from first signal.
%   FPLOTS(STRUCT_DATA, SIGNAL, INDEX) called with three arguments, plots
%   the specified signal and index in arguments SIGNAL and INDEX,
%   respectively.
%   FPLOTS(STRUCT_DATA, SIGNAL, INDEX, MODIFIER) called with four
%   arguments, plots the specifed signal and index with the styled 
%   specified in MODIFIER using MATLAB standard format
%   FPLOTS(STRUCT_DATA, SIGNAL, INDEX, MODIFIER, LINEWIDTH) called with
%   five arguments, plots the specifed signal and index, specified style
%   and line width specified in LINEWIDTH.
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fplots(struct_data, signal, index, modifier, linewidth)

% Create plot
if(nargin ==1)
    plot(struct_data.time,struct_data.signals(1).values(:, :),'LineWidth',1);
elseif(nargin ==3)
    plot(struct_data.time,struct_data.signals(signal).values(:, index),'LineWidth',1);
elseif(nargin ==4)
    plot(struct_data.time,struct_data.signals(signal).values(:, index), modifier, 'LineWidth',1);
elseif(nargin ==5)
    plot(struct_data.time,struct_data.signals(signal).values(:, index), modifier, 'LineWidth',linewidth);
end
axis([-inf, inf, -inf, inf]);
shg;
end