%FPRINTPNG Save the current figure and export for publication
%
%   FPRINTPNG(FILENAME) saves the current figure in FIG format, and export 
%   to PNG for publication using the filename specified in FILENAME. 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fprintpng(filename)

print(filename,'-dpng')

savefig([filename, '.fig'])