%FCONTOURS Plots a set of data from a Z(X,Y) function as a contour plot.
%
%   FCONTOURS(X_MAT, Y_MAT, Z_MAT) plots the function Z_MAT represented by the matrix Y_MAT for Y
%   axis and X_MAT for X axis.
%   FCONTOURS(X_MAT, Y_MAT, Z_MAT, LEVEL) (with LEVEL as a integer) plots the
%   function Z_MAT represented by the matrix Y_MAT for Y axis and X_MAT 
%   for X axis with LEVEL contour curves.
%   FCONTOURS(X_MAT, Y_MAT, Z_MAT, LEVEL) (with LEVEL as a vector) plots the
%   function Z_MAT represented by the matrix Y_MAT for Y axis and X_MAT 
%   for X axis with the specified contour curves in LEVEL vector.
%   FCONTOURS(X_MAT, Y_MAT, Z_MAT, MODIFIER) called with four arguments
%   represents the function with the style specified in MODIFIER using MATLAB standard
%   format
%   FCONTOURS(X_MAT, Y_MAT, Z_MAT, MODIFIER, LINEWIDTH) called with five arguments
%   represents the function with the specified modified and line width
%   specified in LINEWIDTH
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fcontours(x_mat, y_mat, z_mat, level, linewidth)

% Create plot        

if(nargin == 3)
    contObj = contour(x_mat,y_mat, z_mat,'LineWidth',1);
elseif(nargin == 4)
    contObj = contour(x_mat,y_mat, z_mat, level, 'LineWidth',1);
elseif(nargin == 5)
    contObj = contour(x_mat,y_mat, z_mat, level, 'LineWidth', linewidth);
end

clabel(contObj,'FontSize',11,'FontName','times new roman')

axis([-inf, inf,-inf,inf,-inf,inf]);
shg;
end