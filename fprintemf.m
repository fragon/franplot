%FPRINTEMF Save the current figure and export for publication in EMF.
%
%   FPRINTEMF(FILENAME) saves the current figure in FIG format, and export 
%   to EMF for publication using the filename specified in FILENAME. 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fprintemf(filename)

print(filename,'-dmeta')

savefig([filename, '.fig'])