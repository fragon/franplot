%FLABELS Set labels for the active plot
%
%   FLABELS(X_LABEL, Y_LABEL) set the labels for the active plot for the
%   ones specified in X_LABEL for X axis and Y_LABEL for Y axis.
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function flabels(x_label, y_label, z_label)

if nargin == 2
% Create xlabel
xlabel(x_label,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');

% Create ylabel
ylabel(y_label,'LineWidth',1,'FontSize',16,...
    'FontName','times new roman', 'Interpreter', 'latex' );

elseif nargin == 3
    
    % Create xlabel
xlabel(x_label,'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex');

% Create ylabel
ylabel(y_label,'LineWidth',1,'FontSize',16,...
    'FontName','times new roman', 'Interpreter', 'latex' );
% Create zlabel
zlabel(z_label,'LineWidth',1,'FontSize',16,...
    'FontName','times new roman', 'Interpreter', 'latex' );

else
   error('Incorrect number of input arguments'); 
end

end
    
