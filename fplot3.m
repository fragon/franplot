%FPLOT3 Plot a set of data from a Z(X, Y) curve
%
%   FPLOT3(X_VECT, Y_VECT, Z_VECT) plots the curve represented by the vector
%   Z_VECT for Z axis, Y_VECT for Y axis and X_VECT for X axis.
%   FPLOT3(X_VECT, Y_VECT, Z_VECT, MODIFIER) called with four argument
%   represent the curve with the style specified in MODIFIER using MATLAB 
%   standard format
%   FPLOT3(X_VECT, Y_VECT, Z_VECT, MODIFIER, LINEWIDTH) called with five
%   arguments represent the curve with the specified modified and line width
%   specified in LINEWIDTH
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function fplot3(x_vect, y_vect, z_vect, modifier, linewidth)

% Create plot
if(nargin == 3)
    plot3(x_vect,y_vect, z_vect,'LineWidth',1);
elseif(nargin == 4)
    plot3(x_vect,y_vect, z_vect,modifier,'LineWidth',1);
elseif(nargin == 5)
    plot3(x_vect,y_vect, z_vect,modifier,'LineWidth',linewidth);
end

axis([-inf, inf,-inf,inf,-inf,inf]);
shg;
end