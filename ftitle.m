%FTITLE Set the title for a plot
%
%   FTITLE(TITLE_STR) set the title for the active plot to the one 
%   specified in TITLE_STR string. 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function ftitle(title_str)
title(title_str, 'LineWidth',1,'FontSize',16,'FontName','times new roman', 'Interpreter', 'latex')