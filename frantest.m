%FRANTEST
%
%   This script is for developping and learning use only.
%   This script is not intended for production.
%   Running this script will demostrate most of the capabilities
%   implemented in FRANPLOT.
%
%   Each example will run step-by-step, therefore push any key to continue
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

%% 2D data set
x2d = 0:0.01:2*pi; y2d = sin(x2d); 
%3D data set
x3d = 0:0.1:1; y3d = 0:0.1:1; [XX3d, YY3d] = meshgrid(x3d, y3d);
ZZ3d = XX3d .^2 .* YY3d; z3d = sin(x3d + y3d);

%% test 2d single plot
disp("2D single test")
finit();
fplot(x2d, y2d);
pause();
disp("Title test")
ftitle("Prueba sencilla");
pause()
disp("Labels test");
flabels("X lab, $\Lambda$", "Y lab $\Sigma$");
pause();
disp("Second graph in yellow")
fplot(x2d, -y2d, 'y');
pause()
disp("Change upper limit of Y axis and lower of X")
faxis(-0.2, [], [],2.5)
pause()
disp("And plot a third one, with different line size")
fplot(x2d, y2d.^2, 'k', 2)
pause()
disp("Change axis manually")
faxis(-0.1, 2.1*pi, -1.1, 1.4)
pause();
disp("Now add a cool legend")
flegend({'a', '$\lambda$', '$\Gamma$'})
pause();
disp("Now add a column legend")
fcolumnlegend(3, {'a', '$\lambda$', '$\Gamma$'})
pause();
disp("Autoset axis again")
faxis;
pause();
disp("Add a cool text over a graph")
ftext(x2d, y2d, 'Pit $\lambda$')
pause();



%% Test 2D plot subplot.
disp("2D double test")
finit(2,1,1);
fplot(x2d, y2d);
finit(2,1,2);
fplot(x2d, -y2d);
pause();

%% Plot 3D curve
disp("3D test")
finit3;
fplot3(x3d, y3d, z3d);
pause();
disp("Set arbitrary axis")
faxis(-0.2,1.5,-0.2, 1.5, -0.5,[]);
pause();
disp("Restore to tight axis")
faxis();
pause();
disp("Add triple labels")
flabels("X lab, $\Lambda$", "Y lab $\Sigma$",'Z lab, $\Psi$');
pause()

%% Plot 3D mesh and 3D surface
disp("Create a 3d mesh")
finit3(2,1,1);
fmesh(XX3d, YY3d, ZZ3d);
pause();
disp("And down for a 3d surface in calid")
finit3(2,1,2);
C = sin(ZZ3d);
fsurf(XX3d, YY3d, -ZZ3d, C);
pause();
disp('Lets rotate the graph')
faxis([],[],[],[],[],[], 120);
pause();
disp('And now the elevation')
faxis([],[],[],[],[],[], [], 70);
pause();

%% Plot 3D contour
disp("Create a 3d contour")
finit(2,1,1);
fcontours(XX3d, YY3d, ZZ3d, 5);
pause();
finit(2,1,2);
fcontours(XX3d, YY3d, ZZ3d, [0:0.1:1]);
pause();

%% Print test
fprintemf('ftest')
fprintpdf('ftest')
fprinteps('ftest')
fprintpng('ftest')