%FAXIS Set axis to the specified margins
%
%   FAXIS sets the axis to fit the graph tight to the paper
%   FAXIS(XMIN, XMAX, YMIN, YMAX) set the active plot axis to the limits
%   specified in XMIN...XMAX for X axis and YMIN...YMAX for Y axis. Use
%   -inf and +inf to automatic scale the corresponding limit. If any value
%   is left empty, the value is remained from the current configuration.
%   FAXIS(XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX) set the active plot axis to
%   the limits specified in XMIN...XMAX for X axis, YMIN...YMAX for Y
%   axis and ZMIN...ZMAX for Z axis. Use -inf and +inf to automatic scale
%   the corresponding limit. If any value is left empty, the value is
%   remained from the current configuration.
%   FAXIS(XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, AZIMUTH, ELEVATION) called 
%   with extra arguments allow to set view angle of azimuth and elevation
%   to the ones specified in AZIMUTH and ELEVATION. If any value is left 
%   empty, the value is remained from the current configuration 
%
%   Copyright 2020 Francisco J. Gonzalez, Antonio Martin

function faxis(xmin, xmax, ymin, ymax, zmin, zmax, azimuth, elevation)
if(nargin == 0)
    axis([-inf, inf, -inf, inf, -inf, inf]);
elseif nargin == 4
    limsx=get(gca,'XLim');
    limsy=get(gca,'YLim');
    
    if isempty(xmin)
        xmin = limsx(1);
    end
    if isempty(xmax)
        xmax = limsx(2);
    end
    if isempty(ymin)
        ymin = limsy(1);
    end
    if isempty(ymax)
        ymax = limsy(2);
    end
    
    axis([xmin, xmax, ymin, ymax]);
    
elseif nargin >= 6
    limsx=get(gca,'XLim');
    limsy=get(gca,'YLim');
    limsz=get(gca,'ZLim');

    
    if isempty(xmin)
        xmin = limsx(1);
    end
    if isempty(xmax)
        xmax = limsx(2);
    end
    if isempty(ymin)
        ymin = limsy(1);
    end
    if isempty(ymax)
        ymax = limsy(2);
    end
    if isempty(zmin)
        zmin = limsz(1);
    end
    if isempty(zmax)
        zmax = limsz(2);
    end
    
    if nargin > 6
        [az,el] = view;
        if isempty(azimuth)
            azimuth = az;
        end
        
        if nargin < 8 
            elevation = el;
        else
           if isempty(elevation)
               elevation = el;
           end
        end
        view(azimuth, elevation);
    end
    
    axis([xmin, xmax, ymin, ymax, zmin, zmax]);
end
    
    


end